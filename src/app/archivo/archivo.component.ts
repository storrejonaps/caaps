import { Component, OnInit } from '@angular/core';
import { TipoarchivoService } from 'app/services/tipoarchivo.service';
import { ArchivoService } from './archivo.service';

@Component({
  selector: 'app-archivo',
  templateUrl: './archivo.component.html',
  styleUrls: ['./archivo.component.css']
})
export class ArchivoComponent implements OnInit {

  tipoArchivo: any = [];
  entidades: any = [];
  entidadesporArchivos: any = [];
  DEFAULTS: any ={
    _archivo: 'null'
  };
  _archivo: string = 'null';
  mercado: string = 'null';
  ga_archivo: string = 'null'
 
  constructor(    
    public restTipoArchivo:TipoarchivoService,
    public rest:ArchivoService 
    ) { 
      this._archivo = this.DEFAULTS._archivo;

    }

  ngOnInit(): void {
    this.restTipoArchivo.GetLisGrupArch().
        subscribe((data: {}) => {
          this.tipoArchivo = data["data"];
          if(data["data"].length > 0 ){
           // console.log(data["data"]);
           this._archivo = data["data"][0].id;
           //this.mercado = data["data"][0].mercado;
          }
          else{
            this._archivo = this.DEFAULTS._archivo;
          }
          this.changeEntidad(this._archivo);
        });
  }

  changeEntidad(selected){
    console.log('inside changeEntidad (' + selected + ')');
    this.rest.getArchivo(selected).
        subscribe((data: {}) => {
          console.log(data["data"]);
          let ga_archivo = data["data"][0].id;
          let mercado = data["data"][0].mercado;
          console.log('archivo (' + ga_archivo, mercado+ ')');
          this.rest.getEntidad(ga_archivo, mercado).
          subscribe((data: {}) => {
            this.entidades = data["data"];
          });
    });
  }

  changeTipoarchivo(){
    
  }


}
