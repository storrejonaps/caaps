import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { Globals } from 'app/app.globals';
import { Observable } from 'rxjs';

const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

@Injectable({
  providedIn: 'root'
})
export class ArchivoService {
  private url: string = '';

  constructor(private http: HttpClient) {

    this.url = Globals.api;
   }
   getArchivo(_archivo): Observable<any> {
    return this.http.get<any>(`${this.url}/recepcionarchivos/Grupoarchivo/${_archivo}/`)
   }

   getEntidad(ga_archivo, mercado): Observable<any> {
    return this.http.get<any>(`${this.url}/aps/entidades/${ga_archivo}/${mercado}/`)
   }


}
