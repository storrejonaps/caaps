import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Globals } from 'app/app.globals';

const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

@Injectable({
  providedIn: 'root'
})
export class TipoarchivoService {
  private url: string = '';

  constructor(private http: HttpClient) { 
    this.url = Globals.api;
  }

  GetLisGrupArch(): Observable<any> {
    return this.http.get<any>(`${this.url}/recepcionarchivos/ListGrupArch`)
      
  }
  
}
